import sys
import os.path as op
import math
import re

#sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import *
from sqlalchemy.orm import *

from collections import deque

#adc library
from ABE_ADCDACPi import ADCDACPi

#GPIO
import RPi.GPIO as GPIO

# version check and importing tkinter module
if sys.version_info.major == 2:
    import Tkinter as tk
    from Tkinter import *
    from tkFileDialog import *

else:
    import tkinter as tk
    from tkinter import *
    from tkinter.filedialog import *
    from tkinter.ttk import *

from PIL import ImageTk, Image

#natural sort function
def natural_sort(l):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    return sorted(l, key = alphanum_key)

#creating database model and start database
db = create_engine('sqlite:///dig_sig.sqlite')
Base = declarative_base()
metadata = MetaData()

categories_files = Table(
    'categories_files', metadata,
    Column('file_id', Integer(), ForeignKey('file.id')),
    Column('category_id', Integer(), ForeignKey('category.id')),

)



class Category_db(Base):
    __tablename__ = 'Category'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False, unique=True)

    def __str__(self):
        return self.name


class File_db(Base):
    __tablename__ = 'File'
    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    defined_name = Column(String(255), nullable=False)
    expiration_date = Column(DateTime, nullable=False)
    category_id = Column(Integer(), ForeignKey(Category_db.id), nullable=False)
    category = relationship(Category_db, backref='File')

    def __str__(self):
        return self.name

Session = sessionmaker(bind=db)
session = Session()



# main app frame class
class Application(Frame):
    def __init__(self, master=None, dim_miniatures=100, time_interval = 10000):
        Frame.__init__(self, master)
        #dimensions
        self.dim_x = self.winfo_screenwidth()
        self.dim_y = self.winfo_screenheight()
        self.dim_miniatures = dim_miniatures
        #display
        self.num_of_cat_displ = 9
        self.num_of_files_displ = 5

        #rotate information
        self.current_category = 0
        self.current_file = 0
        self.current_page = 0

        #data
        self.categories = deque()
        self.files = deque()
        self.pages = deque()

        #selected flag
        self.categories_selected = False;

        #adc instance
        self.adc = ADCDACPi(1)
        self.adc.__adcrefvoltage = 5

        #setup GPIO
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(18, GPIO.IN) #joystick switch
        self.leds_pins = [3, 5, 7, 11, 13]
        for led_pin in self.leds_pins:
            GPIO.setup(led_pin, GPIO.OUT)
            GPIO.output(led_pin, False)

        #widgets
        self.image = None
        self.image_view = None
        self.text_labels = deque()
        self.text_labels_files = deque()

        self.pack()
        self.createWidgets()

        #load data from database
        self.data_refresh();
        self.reload()

        #periodic event
        self.time_interval = time_interval
        self.worker = self.after(time_interval, self.periodic_update)
        self.periodic_update_joystick()


    def periodic_update_joystick(self):
        GPIO.output(self.leds_pins[2], True)

        #check if button is pressed
        if(not GPIO.input(18)):
            self.categories_selected = not self.categories_selected
            self.reload(reload_image=False)
            self.after_cancel(self.worker)
            self.worker = self.after(60000, self.periodic_update)
            while(not GPIO.input(18)):
                pass

        vertical = self.adc.read_adc_raw(1)
        horizontal = self.adc.read_adc_raw(2)

        if(vertical > 2700):
            if(self.categories_selected):
                self.button_next_cat_callback()
            else:
                self.button_down_callback()

            self.after_cancel(self.worker)
            self.worker = self.after(60000, self.periodic_update)

        elif(vertical < 700):
            if(self.categories_selected):
                self.button_prev_cat_callback()
            else:
                self.button_up_callback()
            self.after_cancel(self.worker)
            self.worker = self.after(60000, self.periodic_update)

        if(horizontal > 2700):
            if(not self.categories_selected):
                self.button_left_callback()
                self.after_cancel(self.worker)
                self.worker = self.after(60000, self.periodic_update)

        elif(horizontal < 700):
            if(not self.categories_selected):
                self.button_right_callback()
                self.after_cancel(self.worker)
                self.worker = self.after(60000, self.periodic_update)

        GPIO.output(self.leds_pins[2], False)
        self.after(200, self.periodic_update_joystick)

            
    def periodic_update(self):
        GPIO.output(self.leds_pins[4], True)
        self.data_refresh()
        if(len(self.files) == self.current_file + 1 or len(self.files) == 0):
            self.button_next_cat_callback()
        else:
            self.button_right_callback()
        GPIO.output(self.leds_pins[3], False)
        self.worker = self.after(self.time_interval, self.periodic_update)

    def reload(self, reload_image=True):
        GPIO.output(self.leds_pins[4], True)
        if(reload_image):
            if(len(self.files) == 0):
                image_name = "files/blank_page.jpg"
            else:
                image_name = self.pages[0]
            opened_image = Image.open(image_name)
            opened_image_size = opened_image.size
            if (self.dim_x/self.dim_y < opened_image_size[0]/opened_image_size[1]):
                opened_image = opened_image.resize((self.dim_x - self.dim_miniatures*2),
                                                    int(opened_image_size[1] * ((self.dim_x - self.dim_miniatures * 2) / opened_image_size[0])),
                                                   Image.ANTIALIAS)
            else:
                opened_image = opened_image.resize((int(opened_image_size[0] * ((self.dim_y - self.dim_miniatures * 2) / opened_image_size[1])),
                                        self.dim_y - self.dim_miniatures*2), Image.ANTIALIAS)

            self.image = ImageTk.PhotoImage(opened_image)
            self.image_view.configure(image=self.image, height=self.master.winfo_height() - self.dim_miniatures,
                                      width=self.master.winfo_width()-self.dim_miniatures*2)



        tmp_categories = self.categories
        for index, text_label in enumerate(self.text_labels, start=-4):
            tmp_categories.rotate(index)
            if(index == 0):
                if(self.categories_selected):
                    text_label.configure(relief='sunken', background='#00529C')
                else:
                    text_label.configure(relief='flat', background='white')
            try:
                text_label.configure(text=tmp_categories[0].name)
            except IndexError:
                text_label.configure(text="Blank")
            tmp_categories.rotate(index * -1)

        tmp_files = self.files
        for index, text_label in enumerate(self.text_labels_files, start=-2):
            tmp_files.rotate(index)
            if(index == 0):
                if(not self.categories_selected):
                    text_label.configure(relief='sunken', background='#00529C')
                else:
                    text_label.configure(relief='flat', background='white')

            try:
                text_label.configure(text=tmp_files[0].defined_name)
            except IndexError:
                text_label.configure(text="Blank")
            tmp_files.rotate(index * -1)


        if(len(self.categories) == 0):
            cat_name = "Blank"
        else:
            cat_name = self.categories[0].name

        if(len(self.files) == 0):
            expiration_date = "Blank"
            file_name = "Blank"
        else:
            expiration_date = self.files[0].expiration_date
            file_name = self.files[0].name
        self.text_label_info.configure(text='Category: {}\nFile: {}\nPage: {}/{}\nExpiration {}'.format(cat_name,
                                                                                         file_name,
                                                                                         self.current_page + 1,
                                                                                         len(self.pages),
                                                                                         expiration_date))
        GPIO.output(self.leds_pins[4], False)

    def data_refresh(self, categories=true, files=true, pages=true):
        GPIO.output(self.leds_pins[1], True)
        if(categories):
            #get categories from DB
            self.categories = deque()
            for instance in session.query(Category_db).order_by(Category_db.name):

                self.categories.append(instance)
            self.categories.rotate(self.current_category);
            files = true;

        if(files):
            #get files for first category in list from DB
            if(len(self.categories) == 0): #if category is blank
                GPIO.output(self.leds_pins[0], False)
                return
            self.files = deque()
            for instance in session.query(File_db).filter_by(category_id=self.categories[0].id).order_by(File_db.name):
                self.files.append(instance)
            self.files.rotate(self.current_file);
            pages = true;

        if(pages):
            if(len(self.files) == 0): #if category is blank
                GPIO.output(self.leds_pins[0], False)
                return
            #get pages for first file
            self.pages = deque()
            file_directory = op.join('files', os.path.splitext(self.files[0].name)[0])
            for (dirpath, dirnames, filenames) in os.walk(file_directory, topdown=True):
                filenames = natural_sort(filenames)
                for filename in filenames:
                    self.pages.append(op.join(file_directory, filename))
            self.pages.rotate(self.current_page);

        GPIO.output(self.leds_pins[1], False)

    def button_left_callback(self):
        self.current_page = 0;
        self.files.rotate(-1)
        self.current_file -= 1
        if(self.current_file < 0):
            self.current_file = len(self.files) - 1
        self.data_refresh(categories=false)
        self.reload()

    def button_right_callback(self):
        self.current_page = 0;
        self.files.rotate(1)
        self.current_file += 1
        if(self.current_file >= len(self.files)):
            self.current_file = 0
        self.data_refresh(categories=false)
        self.reload()

    def button_up_callback(self):
        self.pages.rotate(-1)
        self.current_page += 1
        if(self.current_page >= len(self.pages)):
            self.current_page = 0
        self.reload()

    def button_down_callback(self):
        self.pages.rotate(1)
        self.current_page -= 1
        if(self.current_page < 0):
            self.current_page = len(self.pages) - 1
        self.reload()

    def button_next_cat_callback(self):
        self.current_page = 0;
        self.current_file = 0;
        self.categories.rotate(1)
        self.current_category += 1
        if(self.current_category >= len(self.categories)):
            self.current_category = 0
        self.data_refresh()
        self.reload()

    def button_prev_cat_callback(self):
        self.current_page = 0;
        self.current_file = 0;
        self.categories.rotate(-1)
        self.current_category -= 1
        if(self.current_category < 0):
            self.current_category = len(self.categories) - 1
        self.data_refresh()
        self.reload()

    #key callback
    def key_left(self, event):
        self.button_left_callback()
        self.after_cancel(self.worker)
        self.worker = self.after(self.time_interval * 5, self.periodic_update)

    def key_right(self, event):
        self.button_right_callback()
        self.after_cancel(self.worker)
        self.worker = self.after(self.time_interval * 5, self.periodic_update)

    def key_up(self, event):
        self.button_up_callback()
        self.after_cancel(self.worker)
        self.worker = self.after(self.time_interval * 5, self.periodic_update)

    def key_down(self, event):
        self.button_down_callback()
        self.after_cancel(self.worker)
        self.worker = self.after(self.time_interval * 5, self.periodic_update)

    def key_down_cat(self, event):
        self.button_next_cat_callback()
        self.after_cancel(self.worker)
        self.worker = self.after(self.time_interval * 5, self.periodic_update)

    def key_up_cat(self, event):
        self.button_prev_cat_callback()
        self.after_cancel(self.worker)
        self.worker = self.after(self.time_interval * 5, self.periodic_update)


    # initialization method for widgets
    def createWidgets(self):

        #WIDGET CREATING
        self.image_view = tk.Label(self, height=self.master.winfo_height() - self.dim_miniatures,
                                   width=self.master.winfo_width()-self.dim_miniatures*2,
                                   image=self.image,
                                   background='white',
                                   relief='sunken')
        #self.image_view = Label(self, image=self.image)

        #text labeles
        tmp_categories = self.categories
        for i in range(0, self.num_of_cat_displ):
            if(i == math.ceil(self.num_of_cat_displ/2) - 1):
                text_label = tk.Label(self, relief='sunken', background='#00529C', font=("Arial", 16, "bold"))
            else:
                text_label = tk.Label(self, relief='flat', background='white', font=("Arial", 16))
            self.text_labels.append(text_label)
            tmp_categories.rotate(-1)

        for i in range(0, self.num_of_files_displ):
            if(i == math.ceil(self.num_of_files_displ/2) - 1):
                text_label = tk.Label(self, relief='sunken', background='#00529C', font=("Arial", 16, "bold"))
            else:
                text_label = tk.Label(self, relief='flat', background='white', font=("Arial", 16))
            self.text_labels_files.append(text_label)


        self.text_label_info = Label(self, text="info", background='white', relief='sunken', anchor='center')

        #GRIDS
        self.text_label_info.grid(row=self.num_of_cat_displ, column=self.num_of_files_displ, sticky=N+S+W+E)
        for index, text_label in enumerate(self.text_labels, start=0):
            text_label.grid(row=index, column=self.num_of_files_displ, sticky=N+S+W+E)
        for index, text_label in enumerate(self.text_labels_files, start=0):
            text_label.grid(row=self.num_of_cat_displ, column=index, sticky=N+S+W+E)

        self.image_view.grid(row=0, column=0, columnspan=self.num_of_files_displ, rowspan=self.num_of_cat_displ)

        #panel.pack(side="bottom", fill="both", expand="yes")

        #row config
        self.rowconfigure(self.num_of_cat_displ + 0, minsize=self.dim_miniatures)
        self.columnconfigure(self.num_of_files_displ, minsize=self.dim_miniatures*2)
        
        #bind keys
        self.master.bind("<Left>", self.key_left)
        self.master.bind("<Right>", self.key_right)
        self.master.bind("<Up>", self.key_up)
        self.master.bind("<Down>", self.key_down)
        self.master.bind("<Shift-Up>", self.key_up_cat)
        self.master.bind("<Shift-Down>", self.key_down_cat)

# root = Tk()
# img = ImageTk.PhotoImage(Image.open("images.png"))
# panel = Label(root, image = img)
# panel.pack(side = "bottom", fill = "both", expand = "yes")
# root.mainloop()


root = Tk()
root.title("Digital signage")
root.attributes("-fullscreen", True)
app = Application(master=root)
GPIO.output(app.leds_pins[0], True)
style = Style()
style.theme_use('clam')
#root.bind("<Key>", app.key)
# time.sleep(5)
# app.imageReload()
# app.createWidgets()
app.mainloop()
GPIO.output(app.leds_pins[0], False)