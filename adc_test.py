from ABE_ADCDACPi import ADCDACPi
import time

adc = ADCDACPi(1)
adc.__adcrefvoltage = 5
while True:
    print("CH1: " + str(adc.read_adc_raw(1)) + "\n")
    print("CH2: " + str(adc.read_adc_raw(2)) + "\n")
    time.sleep(1)
